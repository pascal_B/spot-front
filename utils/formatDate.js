export function formatDate(date) {
    const options = {
        year: "numeric",
        month: "long",
        day: "numeric",
    }
    const now = new Date(date).toLocaleDateString("fr-FR", options)
    return now
}
