import { getStrapiMedia } from "utils/media"
import Image from "next/image"
import PropTypes from "prop-types"
import { mediaPropTypes } from "utils/types"

const NextImage = ({ media, toto, ...props }) => {
    const { url, alternativeText } = media

    const loader = ({ src }) => {
        return getStrapiMedia(src)
    }

    // The image has a fixed width and height
    if (props.width && props.height) {
        return (
            <Image
                loader={loader}
                src={url}
                alt={alternativeText || ""}
                {...props}
            />
        )
    }

    // The image has a fixed width and height
    if (props.cover) {
        return (
            <Image
                loader={loader}
                layout="fill"
                src={url}
                className="w-full h-full object-center object-cover overflow-hidden"
                alt={alternativeText || ""}
            />
        )
    }
    // The image is responsive
    return (
        <Image
            loader={loader}
            layout="responsive"
            width={media.width}
            height={media.height}
            objectFit="contain"
            src={url}
            className="w-full h-full"
            alt={alternativeText || ""}
        />
    )
}

Image.propTypes = {
    media: mediaPropTypes,
    className: PropTypes.string,
}

export default NextImage
