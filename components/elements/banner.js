import Markdown from "react-markdown"
import classNames from "classnames"
import { MdClose } from "react-icons/md"
import LocaleSwitch from "../locale-switch"

const Banner = ({ data: { text, type }, closeSelf, viewContext }) => {
    return (
        <div
            className={classNames(
                // Common classes
                "relative z-50 text-gray-800 px-2 py-2",
                {
                    // Apply theme based on notification type
                    "bg-primary-200": type === "info",
                    "bg-orange-600": type === "warning",
                    "bg-red-600": type === "alert",
                }
            )}
        >
            <div className="container flex flex-row justify-between items-center ">
                <div className="rich-text-banner text-sm">
                    <Markdown>{text}</Markdown>
                </div>
                {/* Locale Switch Mobile */}
                {viewContext.localizedPaths && (
                    <div className="">
                        <LocaleSwitch viewContext={viewContext} />
                    </div>
                )}
                {/*<button onClick={closeSelf} className="px-1 py-1 flex-shrink-0">
                    <MdClose className="h-6 w-auto" />
                </button>*/}
            </div>
        </div>
    )
}

export default Banner
