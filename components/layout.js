import { useState } from "react"
import Navbar from "./elements/navbar"
import Footer from "./elements/footer"

const Layout = ({ children, global, viewContext, navigationTheme }) => {
    const { navbar, footer } = global
    return (
        <div className="flex flex-col justify-between min-h-screen">
            {/* Aligned to the top */}
            <div className="flex-1 relative">
                <div className="absolute top-0 w-full z-10">
                    <Navbar navbar={navbar} navigationTheme={navigationTheme} />
                </div>
                {/* page or post content  is here */}
                {children}
                {/* page or post content  is here */}
            </div>
            {/* Aligned to the bottom */}
            <Footer footer={footer} />
        </div>
    )
}

export default Layout
