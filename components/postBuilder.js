import { useRouter } from "next/router"

import CallToActions from "@/components/sections/call-to-actions"
import RichText from "./sections/rich-text"
import Link from "next/link"
import {InView} from "react-intersection-observer";
import classNames from "classnames";


// Map Strapi sections to section components
const sectionComponents = {
    "sections.call-to-actions": CallToActions,
    "sections.rich-text": RichText,
}

// Display a section individually
const Section = ({ sectionData }) => {
    // Prepare the component
    const SectionComponent = sectionComponents[sectionData.__component]
    if (!SectionComponent) {
        return null
    }
    // Display the section
    return <SectionComponent data={sectionData} />
}

// Display the list of sections
const Sections = ({ sections, preview, postAbstractData }) => {
    return (
        <main className="flex flex-col">
            {/* Show a banner if preview mode is on */}
            {preview && <PreviewModeBanner />}
            {/* Back to the blog */}
            <section className="max-w-5xl mx-auto px-4 sm:px-6 lg:px-8 pt-20">
                <nav
                    aria-label="Breadcrumb"
                    className="flex items-center text-gray-500 text-sm font-medium space-x-2 whitespace-nowrap"
                >
                    <Link as={`/`} href="/">
                        <a className="hover:text-gray-900">Accueil</a>
                    </Link>
                    <svg
                        aria-hidden="true"
                        width="24"
                        height="24"
                        fill="none"
                        className="flex-none text-gray-300"
                    >
                        <path
                            d="M10.75 8.75l3.5 3.25-3.5 3.25"
                            stroke="currentColor"
                            strokeWidth="1.5"
                            strokeLinecap="round"
                            strokeLinejoin="round"
                        ></path>
                    </svg>
                    <Link as={`/blog/`} href="blog/">
                        <a className="hover:text-gray-900">Blog</a>
                    </Link>
                </nav>
            </section>
            <section className="sectionHero container flex flex-col md:flex-row items-center justify-between py-12">
                <div className="mx-auto max-w-7xl px-4">
                    <div className="text-center">
                         <InView threshold="1" triggerOnce="true">
                                {({ inView, ref, entry }) => (
                                    <h1
                                        ref={ref}
                                        className={classNames(
                                            "text-4xl font-extrabold tracking-tight  text-gray-800 sm:text-5xl md:text-6xl",
                                            inView ? "is-in-view" : "is-out-view"
                                        )}
                                    >
                                        {postAbstractData.shortName}
                                    </h1>
                                )}
                            </InView>
                         <InView threshold="1" triggerOnce="true">
                                {({ inView, ref, entry }) => (
                                    <p
                                        ref={ref}
                                        className={classNames(
                                            "text-gray-500 mt-3 max-w-md mx-auto text-base sm:text-lg md:mt-5 md:text-xl md:max-w-3xl",
                                            inView ? "is-in-view" : "is-out-view"
                                        )}
                                    >
                                        {postAbstractData.excerpt}

                                    </p>
                                )}
                            </InView>
                    </div>
                </div>
            </section>

            {/* Show the actual sections */}
            {sections.map((section) => (
                <Section
                    sectionData={section}
                    key={`${section.__component}${section.id}`}
                />
            ))}
        </main>
    )
}

const PreviewModeBanner = () => {
    const router = useRouter()
    const exitURL = `/api/exit-preview?redirect=${encodeURIComponent(
        router.asPath
    )}`
    return (
        <div className="py-4 bg-red-600 text-red-100 uppercase">
            <div className="container">
                Preview mode is on.{" "}
                <a
                    className="underline"
                    href={`/api/exit-preview?redirect=${router.asPath}`}
                >
                    Turn off
                </a>
            </div>
        </div>
    )
}

export default Sections
