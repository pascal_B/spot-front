import Link from "next/link"
import { formatDate } from "utils/formatDate"

const allPostsList = ({ data, allPostsData }) => {
    return (
        <div className="container py-12">
            {/*
                <h1 className="text-4xl text-centerborde">{data.title}</h1>
            */}
            <ul className="divide-y divide-gray-200">
                {allPostsData.map((postData) => (
                    <li className="py-12" key={postData.id}>
                        <article className="space-y-2 xl:grid xl:grid-cols-4 xl:space-y-0 xl:items-baseline">
                            <dl>
                                <dt className="sr-only">Published on</dt>
                                <dd className="text-base font-medium text-gray-500">
                                    <time dateTime="{postData.updated_at}">
                                        {formatDate(postData.updated_at)}
                                    </time>
                                </dd>
                            </dl>
                            <div className="space-y-5 xl:col-span-3">
                                <div className="space-y-6">
                                    <h2 className="text-2xl font-bold tracking-tight">
                                        <Link
                                            as={`/blog/article/${postData.slug}`}
                                            href="blog/article/[id]"
                                        >
                                            <a className="text-gray-900">
                                                {postData.shortName}
                                            </a>
                                        </Link>
                                    </h2>
                                    <div className="prose max-w-none text-gray-500">
                                        <div className="prose max-w-none">
                                            <p>{postData.excerpt}</p>
                                        </div>
                                    </div>
                                </div>
                                <div className="text-base font-medium">
                                    <Link
                                        as={`/blog/article/${postData.slug}`}
                                        href="blog/article/[id]"
                                    >
                                        <a
                                            className="text-teal-600 hover:text-teal-700"
                                            aria-label='Read "Introducing Tailwind UI Ecommerce"'
                                        >
                                            Lire la suite →
                                        </a>
                                    </Link>
                                </div>
                            </div>
                        </article>
                    </li>
                ))}
            </ul>
        </div>
    )
}

export default allPostsList
