import { CheckIcon as CheckIconSolid } from "@heroicons/react/solid"

const PricingSpaces = ({ data }) => {
    return (
        <section className="container py-12">
            <div className="">
                <div className="max-w-7xl mx-auto  py-4 sm:py-6 lg:pb-8 px-4 sm:px-6 lg:px-8">
                    <div className="space-y-4 sm:space-y-0 gap-6 sm:grid sm:grid-cols-2 lg:max-w-4xl lg:mx-auto xl:max-w-none xl:mx-0 xl:grid-cols-4 mx-auto">
                        {data.plans.map((plan, index) => (
                            <div key={index}>
                                <div className="py-3 relative text-center">
                                    <h2 className="min-h-7  border-b mt-4">
                                        <span className="block text-sm">
                                            Open Space à la carte
                                        </span>
                                        <span className="text-2xl font-medium text-gray-900 ">
                                            {plan.title}
                                        </span>
                                    </h2>

                                    {plan.prices.map((price, index) => (
                                        <p
                                            key={index}
                                            className="mt-8 text-center"
                                        >
                                            <span className="block text-center text-4xl font-extrabold text-gray-900">
                                                {price.amount} € TTC
                                            </span>
                                            <span className="text-xs font-normal text-gray-500">
                                                {" "}
                                                {price.legend}
                                            </span>
                                        </p>
                                    ))}
                                    <a href="#" target="_self" className="flex w-full justify-center items-center lg:w-auto text-center md:text-lg rounded-md md:py-2 md:px-6 btn-secondary">
                                            Contactez-nous pour réserver
                                    </a>
                                </div>
                                <div className="pt-6 pb-8">
                                    {plan.features.length > 0 && (
                                        <div className="p-4 bg-amber-50 ">
                                            <ul className="space-y-4">
                                                {plan.features.map(
                                                    (feature) => (
                                                        <li
                                                            key={feature.id}
                                                            className="flex space-x-3"
                                                        >
                                                            <CheckIconSolid
                                                                className="flex-shrink-0 h-5 w-5 text-gray-800"
                                                                aria-hidden="true"
                                                            />
                                                            <span className="text-sm text-gray-500">
                                                                {feature.name}
                                                            </span>
                                                        </li>
                                                    )
                                                )}
                                            </ul>
                                        </div>
                                    )}
                                    {plan.extras.length > 0 && (
                                        <div className="p-4 bg-amber-50 mt-3">
                                            <div className="relative mb-4">
                                                <div
                                                    className="absolute inset-0 flex items-center"
                                                    aria-hidden="true"
                                                >
                                                    <div className="w-full border-t border-gray-300"></div>
                                                </div>
                                                <div className="relative flex justify-center">
                                                    <h3 className="px-2 bg-amber-200 text-sm text-gray-800">
                                                        Services extras
                                                    </h3>
                                                </div>
                                            </div>
                                            <ul className="space-y-4">
                                                {plan.extras.map((extra) => (
                                                    <li
                                                        key={extra.id}
                                                        className="flex space-x-3"
                                                    >
                                                        <CheckIconSolid
                                                            className="flex-shrink-0 h-5 w-5 text-gray-800"
                                                            aria-hidden="true"
                                                        />
                                                        <span className="text-sm text-gray-500">
                                                            {extra.name}
                                                        </span>
                                                    </li>
                                                ))}
                                            </ul>
                                        </div>
                                    )}
                                    {plan.options.length > 0 && (
                                        <div className="p-4 mt-3">
                                            <div className="relative mb-4">
                                                <div
                                                    className="absolute inset-0 flex items-center"
                                                    aria-hidden="true"
                                                >
                                                    <div className="w-full border-t border-gray-300"></div>
                                                </div>
                                                <div className="relative flex justify-center">
                                                    <h3 className="px-2 bg-gray-200 text-sm text-gray-80">
                                                        En option
                                                    </h3>
                                                </div>
                                            </div>
                                            <ul className="space-y-4">
                                                {plan.options.map((option) => (
                                                    <li
                                                        key={option.id}
                                                        className="flex space-x-3"
                                                    >
                                                        <CheckIconSolid
                                                            className="flex-shrink-0 h-5 w-5 text-gray-800"
                                                            aria-hidden="true"
                                                        />
                                                        <span className="text-sm text-gray-500">
                                                            {option.name}
                                                        </span>
                                                    </li>
                                                ))}
                                            </ul>
                                        </div>
                                    )}
                                </div>
                            </div>
                        ))}
                    </div>
                </div>
            </div>
            {/*old*/}
        </section>
    )
}

export default PricingSpaces
