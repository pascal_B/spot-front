import { useState } from "react"
import { fetchAPI } from "utils/api"
import * as yup from "yup"
import { Formik, Form, Field } from "formik"
import Button from "../elements/button"

const ContactForm = ({ data }) => {
    const [loading, setLoading] = useState(false)

    const LeadSchema = yup.object().shape({
        email: yup.string().email().required(),
    })

    return (
        <section className="py-10">
            <div className="container">
                <div className="px-6 py-6">
                    <div className="xl:w-0 xl:flex-1 text-center xl:text-left">
                        <h2 className="text-2xl font-extrabold tracking-tight text-white sm:text-3xl">
                            {data.title}
                        </h2>
                        <p className="mt-3 max-w-3xl text-lg leading-6 text-emerald-50">
                            {data.description}
                        </p>
                    </div>
                    <Formik
                        initialValues={{ email: "" }}
                        validationSchema={LeadSchema}
                        onSubmit={async (
                            values,
                            { setSubmitting, setErrors }
                        ) => {
                            setLoading(true)
                            try {
                                setErrors({ api: null })
                                await fetchAPI("/contact-form-submissions", {
                                    method: "POST",
                                    body: JSON.stringify({
                                        firstName: values.firstName,
                                        lastName: values.lastName,
                                        email: values.email,
                                        textArea: values.textArea,
                                    }),
                                })
                            } catch (err) {
                                setErrors({ api: err.message })
                            }

                            setLoading(false)
                            setSubmitting(false)
                        }}
                    >
                        {({ errors, touched, isSubmitting }) => (
                            <div className="mt-8">
                                <Form className="mt-6 grid grid-cols-1 gap-y-2 gap-x-4 sm:grid-cols-12">
                                    <div className="sm:col-span-6">
                                        <label htmlFor="firstName" className="block text-sm font-medium text-gray-700">
                                           Nom
                                        </label>
                                        <Field
                                            className="w-full border-gray-500 px-5 py-3 placeholder-gray-500 focus:outline-none  focus:border-gray-700 rounded-md"
                                            type="text"
                                            name="firstName"
                                            placeholder={data.firstNamePlaceholder}
                                        />
                                    </div>
                                    <div className="sm:col-span-6">
                                        <label htmlFor="lastName" className="block text-sm font-medium text-gray-700">
                                           Prénom
                                        </label>
                                        <Field
                                            className="w-full border-gray-500 px-5 py-3 placeholder-gray-500 focus:outline-none  focus:border-gray-700 rounded-md"
                                            type="text"
                                            name="lastName"
                                            placeholder={data.lastNamePlaceholder}
                                        />
                                    </div>

                                    <div className="sm:col-span-12">
                                        <label htmlFor="email" className="block text-sm font-medium text-gray-700">
                                            Adresse email
                                        </label>
                                        <Field
                                            className="w-full border-gray-500 px-5 py-3 placeholder-gray-500 focus:outline-none  focus:border-gray-700 rounded-md"
                                            type="email"
                                            name="email"
                                            placeholder={data.emailPlaceholder}
                                        />
                                    </div>
                                    <div className="sm:col-span-12">
                                        <label htmlFor="textArea" className="block text-sm font-medium text-gray-700">
                                           Message
                                        </label>
                                        <Field
                                            className="w-full border-gray-500 px-5 py-3 placeholder-gray-500 focus:outline-none  focus:border-gray-700 rounded-md"
                                            as="textarea"
                                            name="textArea"
                                            placeholder={data.textAreaPlaceholder}
                                        />
                                    </div>
                                    <div className="sm:col-span-12">
                                        <Button
                                            type="submit"
                                            button={data.submitButton}
                                            key={data.submitButton.id}
                                            disabled={isSubmitting}
                                            loading={loading}
                                            theme={data.submitButton.type}
                                        />
                                    </div>
                                </Form>
                                <p className="text-red-500 h-12 text-sm mt-1 ml-2 text-left">
                                    {(errors.email &&
                                        touched.email &&
                                        errors.email) ||
                                    errors.api}
                                </p>
                            </div>
                        )}
                    </Formik>
                </div>
            </div>
        </section>
    )
}

export default ContactForm
