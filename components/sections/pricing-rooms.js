import { CheckIcon as CheckIconSolid } from "@heroicons/react/solid"

const Pricing = ({ data }) => {
    return (
        <section className="container py-12">
            <div className="">
                <div className="max-w-7xl mx-auto  py-4 sm:py-6 lg:pb-8 px-4 sm:px-6 lg:px-8">
                    <div className="bg-primary-100 py-20">
                        <div className="space-y-4 sm:space-y-0 sm:grid sm:grid-cols-2 sm:gap-6 lg:max-w-4xl lg:mx-auto xl:max-w-none xl:mx-0 mx-auto">
                            <div className="">
                                <h2 className="block text-4xl font-extrabold text-gray-900">
                                    {data.title}
                                </h2>
                                <p className="text-white font-medium">
                                    {data.description}
                                </p>
                                <div className="my-4">
                                    <a
                                        href="#"
                                        target="_self"
                                        className="ml-0 text-center md:text-lg rounded-md md:py-2 md:px-6 btn-white"
                                    >
                                        Contactez-nous pour réserver
                                    </a>
                                </div>
                            </div>
                            <div>
                                {data.plans.map((plan, index) => (
                                    <div key={index}>
                                        <h2>{plan.title}</h2>
                                        <ul className="divide-y divide-gray-200">
                                            {plan.prices.map((price, index) => (
                                                <li
                                                    key={index}
                                                    className="flex"
                                                >
                                                    <div>{price.legend}</div>
                                                    <div>
                                                        {price.amount} € TTC
                                                    </div>
                                                </li>
                                            ))}
                                        </ul>
                                    </div>
                                ))}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {/*old*/}
        </section>
    )
}

export default Pricing
