import NextImage from "../elements/image"
import classNames from "classnames"
import { InView } from "react-intersection-observer"
import CustomLink from "@/components/elements/custom-link"

const FeatureColumnsGroup = ({ data }) => {
    return (
        <section className="container flex flex-col gap-12 py-12">
            <div className="relative lg:pl-5 text-center">
                {data.label && (
                    <InView threshold="1" triggerOnce="true">
                        {({ inView, ref, entry }) => (
                            <div
                                ref={ref}
                                ref={ref}
                                className={classNames(
                                    "line-wrapper relative mx-auto",
                                    inView ? "is-in-view" : "is-out-view"
                                )}
                            >
                                <span className="relative line-element sm:text-lg font-semibold mb-3">
                                    {data.label}
                                </span>
                            </div>
                        )}
                    </InView>
                )}
                {data.title && (
                    <InView threshold="1" triggerOnce="true">
                        {({ inView, ref, entry }) => (
                            <h1
                                ref={ref}
                                className={classNames(
                                    "text-3xl font-extrabold tracking-tight text-gray-800 sm:text-5xl md:text-5xl mx-auto max-w-md md:max-w-3xl",
                                    inView ? "is-in-view" : "is-out-view"
                                )}
                            >
                                {data.title}
                            </h1>
                        )}
                    </InView>
                )}
                {data.description && (
                    <InView threshold="1" triggerOnce="true">
                        {({ inView, ref, entry }) => (
                            <p
                                ref={ref}
                                className={classNames(
                                    "text-gray-500 mt-3 md:mt-5 text-base sm:text-lg md:text-xl mx-auto max-w-md md:max-w-3xl",
                                    inView ? "is-in-view" : "is-out-view"
                                )}
                            >
                                {data.description}
                            </p>
                        )}
                    </InView>
                )}
            </div>
            <div className="flex flex-col lg:flex-row lg:flex-wrap gap-12 align-top py-12 px-5">
                {data.features.map((feature, index) => (
                    <InView threshold="1" triggerOnce="true" key={index}>
                        {({ inView, ref, entry }) => (
                            <div
                                ref={ref}
                                className={classNames(
                                    "flex-1 text-lg bg-gray-50 p-4 border border-gray-100",
                                    inView ? "is-in-view" : "is-out-view"
                                )}
                            >
                                <div className="w-10 h-10">
                                    <NextImage media={feature.icon} />
                                </div>
                                <h3 className="font-bold mt-4 mb-4">
                                    {feature.title}
                                </h3>
                                <p>{feature.description}</p>
                            </div>
                        )}
                    </InView>
                ))}
            </div>
        </section>
    )
}

export default FeatureColumnsGroup
