import Markdown from "react-markdown"
import ButtonLink from "../elements/button-link"
import classNames from "classnames"
import { InView } from "react-intersection-observer"

const Hero = ({ data }) => {
    return (
        <section className="sectionHero container flex flex-col md:flex-row items-center justify-between py-12">
            <div className="mx-auto max-w-7xl px-4">
                <div className="text-center">
                    {data.title && (
                        <InView threshold="1" triggerOnce="true">
                            {({ inView, ref, entry }) => (
                                <h1
                                    ref={ref}
                                    className={classNames(
                                        "text-4xl font-extrabold tracking-tight  text-gray-800 sm:text-5xl md:text-6xl",
                                        inView ? "is-in-view" : "is-out-view"
                                    )}
                                >
                                    {data.title}
                                </h1>
                            )}
                        </InView>
                    )}
                    {data.description && (
                        <InView threshold="1" triggerOnce="true">
                            {({ inView, ref, entry }) => (
                                <p
                                    ref={ref}
                                    className={classNames(
                                        "text-gray-500 mt-3 max-w-md mx-auto text-base sm:text-lg md:mt-5 md:text-xl md:max-w-3xl",
                                        inView ? "is-in-view" : "is-out-view"
                                    )}
                                >
                                    {data.description}
                                </p>
                            )}
                        </InView>
                    )}
                    {data.buttons && (
                        <div className="mt-5 mx-auto flex flex-col sm:flex-row sm:flex sm:justify-center gap-4 gap-y-2">
                            {data.buttons.map((button, index) => (
                                <ButtonLink
                                    button={button}
                                    theme={button.type}
                                    key={index}
                                />
                            ))}
                        </div>
                    )}
                    {data.smallTextWithLink && (
                        <div className="text-base md:text-sm mt-4 sm:mt-3 rich-text-hero">
                            <Markdown>{data.smallTextWithLink}</Markdown>
                        </div>
                    )}
                </div>
            </div>
        </section>
    )
}

export default Hero
