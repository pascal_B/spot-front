import classNames from "classnames"
import NextImage from "../elements/image"
import Video from "../elements/video"
import CustomLink from "../elements/custom-link"
import { InView } from "react-intersection-observer"

const FeatureRowsGroup = ({ data }) => {
    return (
        <section className="container flex flex-col gap-12 py-12">
            {data.features.map((feature, index) => (
                <div
                    className="relative lg:grid lg:grid-cols-2 lg:gap-8 lg:items-center"
                    key={index}
                >
                    <div className="relative lg:pl-5">
                        {feature.label && (
                            <InView threshold="1" triggerOnce="true">
                                {({ inView, ref, entry }) => (
                                    <div
                                        ref={ref}
                                        ref={ref}
                                        className={classNames(
                                            "line-wrapper relative",
                                            inView
                                                ? "is-in-view"
                                                : "is-out-view"
                                        )}
                                    >
                                        <span className="line-element sm:text-lg font-semibold  mb-3">
                                            {feature.label}
                                        </span>
                                    </div>
                                )}
                            </InView>
                        )}
                        {feature.title && (
                            <InView threshold="1" triggerOnce="true">
                                {({ inView, ref, entry }) => (
                                    <h1
                                        ref={ref}
                                        className={classNames(
                                            "text-3xl font-extrabold tracking-tight text-gray-800 sm:text-5xl md:text-5xl",
                                            inView
                                                ? "is-in-view"
                                                : "is-out-view"
                                        )}
                                    >
                                        {feature.title}
                                    </h1>
                                )}
                            </InView>
                        )}
                        {feature.description && (
                            <InView threshold="1" triggerOnce="true">
                                {({ inView, ref, entry }) => (
                                    <p
                                        ref={ref}
                                        className={classNames(
                                            "text-gray-500 mt-3 max-w-md mx-auto text-base sm:text-lg md:mt-5 md:text-xl md:max-w-3xl",
                                            inView
                                                ? "is-in-view"
                                                : "is-out-view"
                                        )}
                                    >
                                        {feature.description}
                                    </p>
                                )}
                            </InView>
                        )}
                        {feature.link && (
                            <CustomLink link={feature.link}>
                                <div className="text-primary-400 hover:underline">
                                    {feature.link.text}
                                </div>
                            </CustomLink>
                        )}
                    </div>
                    <div
                        className={classNames(
                            "lg:pr-5 my-20 relative  w-full p-10 h-114 m- self-center",
                            { "order-first": index % 2 === 1 }
                        )}
                        aria-hidden="true"
                    >
                        <div className="bg-amber-100 absolute -top-20 -bottom-20 left-10 right-10 z-0"></div>
                        <div className="flex justify-center">
                            {/* Images */}
                            {feature.media.mime.startsWith("image") && (
                                <NextImage
                                    media={feature.media}
                                    cover
                                    className="w-full h-full object-center object-cover"
                                />
                            )}
                            {/* Videos */}
                            {feature.media.mime.startsWith("video") && (
                                <Video
                                    media={feature.media}
                                    className="bg-gray-100 white mx-auto z-10"
                                    autoPlay
                                    controls={false}
                                />
                            )}
                        </div>
                    </div>
                </div>
            ))}
        </section>
    )
}

export default FeatureRowsGroup
