import { useRouter } from "next/router"
import Hero from "@/components/sections/hero"
import HeroSlider from "@/components/sections/hero-slider"
import LargeVideo from "@/components/sections/large-video"
import FeatureColumnsGroup from "@/components/sections/feature-columns-group"
import FeatureRowsGroup from "@/components/sections/feature-rows-group"
import CallToActions from "@/components/sections/call-to-actions"
import TestimonialsGroup from "@/components/sections/testimonials-group"
import RichText from "./sections/rich-text"
import PricingSpaces from "./sections/pricing-spaces"
import PricingRooms from "./sections/pricing-rooms"
import Overview from "./sections/overview"
import Gallery from "./sections/gallery"
import NewsletterForm from "./sections/newsletter-form"
import ContactForm from "./sections/contact-form"
import AllPostList from "./sections/all-posts-list"
import HighlightedPostList from "./sections/highlighted-posts-list"

// Map Strapi sections to section components
const sectionComponents = {
    "sections.hero": Hero,
    "sections.hero-slider": HeroSlider,
    "sections.large-video": LargeVideo,
    "sections.feature-columns-group": FeatureColumnsGroup,
    "sections.feature-rows-group": FeatureRowsGroup,
    "sections.call-to-actions": CallToActions,
    "sections.testimonials-group": TestimonialsGroup,
    "sections.rich-text": RichText,
    "sections.pricing-spaces": PricingSpaces,
    "sections.pricing-rooms": PricingRooms,
    "sections.overview": Overview,
    "sections.gallery": Gallery,
    "sections.newsletter-form": NewsletterForm,
    "sections.contact-form": ContactForm,
    "sections.all-posts-list": AllPostList,
    "sections.highlighted-posts-list": HighlightedPostList,
}

// Display a section individually
const Section = ({ sectionData, allPostsData, highlightedPostsData }) => {
    // Prepare the component
    const SectionComponent = sectionComponents[sectionData.__component]
    if (!SectionComponent) {
        return null
    }
    // Display the section
    return (
        <SectionComponent
            data={sectionData}
            allPostsData={allPostsData}
            highlightedPostsData={highlightedPostsData}
        />
    )
}

// Display the list of sections
const Sections = ({
    sections,
    preview,
    allPostsData,
    highlightedPostsData,
}) => {
    return (
        <main className="flex flex-col">
            {/* Show a banner if preview mode is on */}
            {preview && <PreviewModeBanner />}
            {/* Show the actual sections */}
            {sections.map((section) => (
                <Section
                    allPostsData={allPostsData}
                    highlightedPostsData={highlightedPostsData}
                    sectionData={section}
                    key={`${section.__component}${section.id}`}
                />
            ))}
        </main>
    )
}

const PreviewModeBanner = () => {
    const router = useRouter()
    const exitURL = `/api/exit-preview?redirect=${encodeURIComponent(
        router.asPath
    )}`
    return (
        <div className="py-4 bg-red-600 text-red-100 uppercase">
            <div className="container">
                Preview mode is on.{" "}
                <a
                    className="underline"
                    href={`/api/exit-preview?redirect=${router.asPath}`}
                >
                    Turn off
                </a>
            </div>
        </div>
    )
}

export default Sections
