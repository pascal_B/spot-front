import { Fragment, useEffect, useState, useRef } from "react"
import { useRouter } from "next/router"
import PropTypes from "prop-types"
import { Menu, Transition } from "@headlessui/react"
import Cookies from "js-cookie"
import { MdExpandMore } from "react-icons/md"
import WorldIcon from "./icons/world"

const LocaleSwitch = ({ viewContext }) => {
    const isMounted = useRef(false)
    const router = useRouter()
    const [locale, setLocale] = useState()

    const handleLocaleChange = async (selectedLocale) => {
        // Persist the user's language preference
        // https://nextjs.org/docs/advanced-features/i18n-routing#leveraging-the-next_locale-cookie
        Cookies.set("NEXT_LOCALE", selectedLocale)
        setLocale(selectedLocale)
    }

    const handleLocaleChangeRef = useRef(handleLocaleChange)

    useEffect(() => {
        const localeCookie = Cookies.get("NEXT_LOCALE")
        if (!localeCookie) {
            handleLocaleChangeRef.current(router.locale)
        }

        const checkLocaleMismatch = async () => {
            if (
                !isMounted.current &&
                localeCookie &&
                localeCookie !== viewContext.locale
            ) {
                // Redirect to locale page if locale mismatch
                const localeView = getLocalizedView(localeCookie, viewContext)

                router.push(
                    `${localizePath({ ...viewContext, ...localeView })}`,
                    `${localizePath({ ...viewContext, ...localeView })}`,
                    { locale: localeView.locale }
                )
            }
        }
        setLocale(localeCookie || router.locale)
        checkLocaleMismatch()

        return () => {
            isMounted.current = true
        }
    }, [locale, router, viewContext])

    return (
        <Menu as="div" className="ml-4 relative">
            <div>
                <Menu.Button className="flex items-center btn btn-white focus:outline-none">
                    <span className="sr-only">lang</span>
                    <WorldIcon />
                    <p>{locale}</p>
                    <MdExpandMore className="ml-1" />
                </Menu.Button>
            </div>
            <Transition
                as={Fragment}
                enter="transition ease-out duration-100"
                enterFrom="transform opacity-0 scale-95"
                enterTo="transform opacity-100 scale-100"
                leave="transition ease-in duration-75"
                leaveFrom="transform opacity-100 scale-100"
                leaveTo="transform opacity-0 scale-95"
            >
                <div className="divide-y-1 divide-gray-100 origin-top-right absolute right-0 mt-2 w-48 rounded-md shadow-lg py-1 bg-white ring-1 ring-black ring-opacity-5 focus:outline-none">
                    {viewContext.localizedPaths &&
                        viewContext.localizedPaths.map(({ href, locale }) => {
                            return (
                                <Menu.Item key={locale} locale={locale}>
                                    <a
                                        href={href}
                                        role="option"
                                        className="block hover:bg-gray-100 px-4 py-2 text-sm text-gray-700"
                                        onClick={() =>
                                            handleLocaleChange(locale)
                                        }
                                    >
                                        {locale}
                                    </a>
                                </Menu.Item>
                            )
                        })}
                </div>
            </Transition>
        </Menu>
    )
}

LocaleSwitch.propTypes = {
    initialLocale: PropTypes.string,
}

export default LocaleSwitch
