const defaultTheme = require("tailwindcss/defaultTheme")
const colors = require("tailwindcss/colors")

module.exports = {
    mode: "jit", // see https://tailwindcss.com/docs/just-in-time-mode
    purge: [
        "./pages/**/*.{js,ts,jsx,tsx}",
        "./components/**/*.{js,ts,jsx,tsx}",
    ],
    darkMode: false, // or "media" or "class"
    theme: {
        colors: {
            "primary-100": "#96D1CB",
            "primary-200": "#96D1CB",
            transparent: "transparent",
            current: "currentColor",
            black: colors.black,
            white: colors.white,
            gray: colors.trueGray,
            indigo: colors.indigo,
            red: colors.rose,
            yellow: colors.yellow,
            amber: colors.amber,
            blue: colors.blue,
            sky: colors.sky,
            green: colors.green,
            emerald: colors.emerald,
        },
        fontSize: {
            xs: ["0.75rem", { lineHeight: "1rem" }],
            sm: ["0.875rem", { lineHeight: "1.25rem" }],
            base: ["1rem", { lineHeight: "1.5rem" }],
            lg: ["1rem", { lineHeight: "1.5rem" }],
            xl: ["1.25rem", { lineHeight: "1.75rem" }],
            "2xl": ["1.5rem", { lineHeight: "2rem" }],
            "3xl": ["1.875rem", { lineHeight: "2.25rem" }],
            "4xl": ["2.25rem", { lineHeight: "2.5rem" }],
            "5xl": ["3rem", { lineHeight: "1" }],
            "6xl": ["3.75rem", { lineHeight: "1" }],
            "7xl": ["4.5rem", { lineHeight: "1" }],
            "8xl": ["6rem", { lineHeight: "1" }],
            "9xl": ["8rem", { lineHeight: "1" }],
        },
        extend: {
            height: {
                114: "28.5rem",
            },
            minHeight: {
                '7': '4.5rem'
            },
            colors: {
                //primary: colors.emerald,
                secondary: colors.blue,
                "primary-dark": colors.emerald,
            },
            fontWeight: ["hover", "focus"],
            fontFamily: {
                sans: ["Inter var", ...defaultTheme.fontFamily.sans],
            },
            container: {
                center: true,
            },
        },
        screens: {
            sm: "640px",
            md: "768px",
            lg: "1024px",
            xl: "1280px",
        },
    },
    variants: {
        extend: {},
    },
    plugins: [
        require("@tailwindcss/typography"),
        require("@tailwindcss/forms"),
        require("@tailwindcss/aspect-ratio"),
    ],
}
