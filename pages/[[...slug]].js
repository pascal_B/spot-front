import ErrorPage from "next/error"
import {
    getPageData,
    fetchAPI,
    getGlobalData,
    getAllPostsData,
    getHighlightedPostsData,
} from "utils/api"
import Sections from "@/components/pageBuilder"
import Seo from "@/components/elements/seo"
import { useRouter } from "next/router"
import Layout from "@/components/layout"
import { getLocalizedPaths } from "utils/localize"

// The file is called [[...slug]].js because we're using Next's
// optional catch all routes feature. See the related docs:
// https://nextjs.org/docs/routing/dynamic-routes#optional-catch-all-routes

const DynamicPage = ({
    highlightedPostsData,
    allPostsData,
    sections,
    metadata,
    preview,
    global,
    viewContext,
    navigationTheme,
}) => {
    const router = useRouter()

    // Check if the required data was provided
    if (!router.isFallback && !sections?.length) {
        return <ErrorPage statusCode={404} />
    }

    // Loading screen (only possible in preview mode)
    if (router.isFallback) {
        return <div className="container">Loading...</div>
    }

    // Build  page render
    return (
        <Layout
            global={global}
            viewContext={viewContext}
            navigationTheme={navigationTheme}
        >
            {/* Update default metadata */}
            <Seo metadata={metadata} />
            {/* Display content sections */}
            <Sections
                sections={sections}
                preview={preview}
                allPostsData={allPostsData}
                highlightedPostsData={highlightedPostsData}
            />
        </Layout>
    )
}

export async function getStaticPaths(context) {
    // Get all pages from Strapi
    const allPages = context.locales.map(async (locale) => {
        const localePages = await fetchAPI(`/pages?_locale=${locale}`)
        return localePages
    })
    const pages = await (await Promise.all(allPages)).flat()
    const paths = pages.map((page) => {
        // Decompose the slug that was saved in Strapi
        const slugArray = !page.slug ? false : page.slug.split("/")

        return {
            params: { slug: slugArray },
            // Specify the locale to render
            locale: page.locale,
        }
    })
    return { paths, fallback: true }
}

export async function getStaticProps(context) {
    const { params, locale, locales, defaultLocale, preview = null } = context

    const globalLocale = await getGlobalData(locale)

    // Fetch pages. Include drafts if preview mode is on
    const pageData = await getPageData(
        { slug: !params.slug ? [""] : params.slug },
        locale,
        preview
    )
    if (pageData == null) {
        // Giving the page no props will trigger a 404 page
        return { props: {} }
    }
    // We have the required page data, pass it to the page component
    const { navigationTheme, contentSections, metadata, localizations, slug } =
        pageData
    // Fetch all posts. Include drafts if preview mode is on
    const allPostsData = await getAllPostsData(locale, preview)
    // Fetch highlighted Posts. Include drafts if preview mode is on
    const highlightedPostsData = await getHighlightedPostsData(locale, preview)
    const viewContext = {
        locale: pageData.locale,
        locales,
        defaultLocale,
        slug,
        localizations,
    }
    const localizedPaths = getLocalizedPaths(viewContext)
    return {
        props: {
            params,
            sections: contentSections,
            navigationTheme,
            metadata,
            preview,
            global: globalLocale,
            viewContext: {
                ...viewContext,
                localizedPaths,
            },
            highlightedPostsData,
            allPostsData,
        },
    }
}

export default DynamicPage
